import React from "react";
import Header from "./components/header/header";
import HomePage from "./components/homepage/homepage";
import SearchResults from "./components/search-results/search-results";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

import "./App.scss";
function App() {
  return (
    <Router>
      <div className="app-wrapper">
        <Header />
        <section className="body-wrapper">
          <Switch>
            <Route exact path="/">
              <HomePage />
            </Route>
            <Route path="/search/:searchTerm">
              <SearchResults />
            </Route>
          </Switch>
        </section>
      </div>
    </Router>
  );
}

export default App;
