export const randomInt = (min = 100, max = 200) => {
  return Math.floor(Math.random() * (max - min + 1) + min);
};

export const delay = (ms = randomInt()) => {
  return new Promise((resolve) => setTimeout(resolve, ms));
};

export const debounce = (f, interval = 400) => {
  let timer = null;

  return (...args) => {
    clearTimeout(timer);
    return new Promise((resolve) => {
      timer = setTimeout(() => resolve(f(...args)), interval);
    });
  };
};
export const checkIfTwoArraysHasCommon = (arr1, arr2) => {
  for (let i = 0; i < arr1.length; i++) {
    for (let j = 0; j < arr2.length; j++) {
      if (arr1[i] === arr2[j]) {
        return true;
      }
    }
  }

  return false;
};
