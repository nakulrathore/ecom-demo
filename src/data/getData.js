import mockData from "./mockData.js";

import {
  delay,
  debounce,
  checkIfTwoArraysHasCommon,
  randomInt,
} from "../helper-functions";

export const getProductByGroupsData = debounce((groupNames = []) => {
  return delay(randomInt()).then(() => {
    const groupedData = [];
    for (const [productKey, productData] of Object.entries(mockData)) {
      if (groupNames.includes(productKey)) {
        groupedData.push(productData);
      }
    }
    return groupedData;
  });
});
export const getSearchResults = debounce((query) => {
  const looseQueries = query.toLowerCase().split(" ");
  console.log(looseQueries);
  return delay(randomInt()).then(() => {
    const result = [];
    for (const [, productData] of Object.entries(mockData)) {
      productData.products.forEach((product) => {
        const titleKeywords = product.title.toLocaleLowerCase().split(" ");

        if (checkIfTwoArraysHasCommon(looseQueries, titleKeywords)) {
          result.push(product);
        } else {
          // handle when not found in one set
        }
      });
    }
    const hasResults = result.length;
    return {
      data: hasResults
        ? result
        : "no result found, try searching oneplus/apple/nord/iphone",
      code: hasResults ? 200 : 203,
    };
  });
});
