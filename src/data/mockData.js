const mockData = {
  iphone: {
    category: 0,
    productGroupDesc: "Apple fan? buy an Iphone",
    products: [
      {
        id: 0,
        title: "Apple iphone 11",
        desc: "by Apple Inc.",
        cost: "64,999",
        images: ["iphone11-0", "iphone11-1"],
      },
      {
        id: 1,
        title: "Apple iphone XR",
        desc: "by Apple Inc.",
        cost: "44,999",
        images: ["iphonexr-0", "iphonexr-1"],
      },
      {
        id: 2,
        title: "Apple iphone 8",
        desc: "by Apple Inc.",
        cost: "24,999",
        images: ["iphone8-0", "iphone8-1"],
      },
    ],
  },
  oneplus_phone: {
    category: 1,
    productGroupDesc: "Android fan? buy a OnePlus",
    products: [
      {
        id: 0,
        title: "OnePlus 9 Pro",
        desc: "by OnePlus Inc.",
        cost: "64,999",
        images: ["oneplus9-0", "oneplus9-1"],
      },
      {
        id: 1,
        title: "OnePlus 8T",
        desc: "by OnePlus Inc.",
        cost: "64,999",
        images: ["oneplus8t-0", "oneplus8t-1"],
      },
      {
        id: 2,
        title: "OnePlus Nord",
        desc: "by OnePlus Inc.",
        cost: "64,999",
        images: ["oneplusnord-0", "oneplusnord-1"],
      },
    ],
  },
};

export default mockData;
