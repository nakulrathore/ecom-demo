// import "./se.scss";
import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";


import { getSearchResults } from "../../data/getData";
import ProductCard from "../../components/product-card/product-card";

function SearchResults() {
  const [searchData, setSearchData] = useState({
    isLoading: true,
    data: [],
  });

  let { searchTerm } = useParams();

  useEffect(() => {
    getSearchResults(searchTerm).then((data) => {
      setSearchData({
        isLoading: false,
        data,
      });
    });
  }, [searchTerm]);

  const renderProducts = (products) => {
    return (
      <div className="product-group">
        <h3>Showing results for {searchTerm}</h3>
        <section className="product-list">
          {products.map((product) => {
            const { title, cost, images } = product;
            const productImage = require(`../../assets/images/${images[0]}.jpg`)
              .default;

            return (
              <ProductCard
                title={title}
                cost={cost}
                productImage={productImage}
              />
            );
          })}
        </section>
      </div>
    );
  };

  const { isLoading, data } = searchData;
  const isDataAvailable = "code" in data;

  return (
    <section className="body-wrapper">
      {isLoading ? (
        <div class="loader"></div>
      ) : isDataAvailable && data.code === 200 ? (
        renderProducts(data.data)
      ) : isDataAvailable && data.code === 203 ? (
        <div className="no-data">{data.data}</div>
      ) : null}
    </section>
  );
}

export default SearchResults;
