import React, { useState, useEffect } from "react";
import { useHistory } from "react-router-dom";

import SearchResultDropDown from "./searchResultDropdown";
import { getSearchResults } from "../../data/getData";

import "./search.scss";
function SearchBar(props) {
  const history = useHistory(props);

  const [query, setQuery] = useState("");
  const [isLoading, setIsLoading] = useState(false);
  const [searchResults, setSearchResults] = useState({});

  useEffect(() => {
    setSearchResults([]);
    if (query.length) {
      setIsLoading(true);
      getSearchResults(query)
        .then((result) => {
          setSearchResults(result);
        })
        .finally(() => {
          setIsLoading(false);
        });
    } else {
      setIsLoading(false);
    }
  }, [query]);

  const handleQueryInput = (event) => {
    const query = event.target.value;
    setQuery(query);
  };

  const handleKeyDown = (event) => {
    if (event.key === "Enter") {
      setQuery("");
      history.push(`/search/${query}`);
    }
  };

  const isDataAvailable = "code" in searchResults;
  const isDropdownVisible = isDataAvailable || isLoading;
  console.log("isDropdownVisible", isDropdownVisible, isDataAvailable);

  return (
    <div className="search-wrapper">
      <input
        value={query}
        type="text"
        placeholder="e.g. iphone or Nord"
        required="required"
        onChange={handleQueryInput}
        onKeyDown={handleKeyDown}
        className={`${isDropdownVisible ? "dropdown-visible" : null} `}
      />

      <SearchResultDropDown
        searchResults={searchResults}
        isLoading={isLoading}
      />
    </div>
  );
}

export default SearchBar;
