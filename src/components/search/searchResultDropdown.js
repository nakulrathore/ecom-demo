import React, { memo } from "react";

function searchResultDropDown({ searchResults, isLoading }) {
  const isDataAvailable = "code" in searchResults;
  const isDropdownVisible = isDataAvailable || isLoading;

  return (
    <div
      className={`search-dropdown-wrapper ${
        isDropdownVisible ? "visible" : "hidden"
      }`}
    >
      {isLoading ? (
        <div class="loader"></div>
      ) : isDataAvailable && searchResults.code === 200 ? (
        searchResults.data.map((result) => {
          const { title, id } = result;
          return (
            <div key={id} className="result-item">
              {title}
            </div>
          );
        })
      ) : isDataAvailable && searchResults.code === 203 ? (
        <div className="no-data">{searchResults.data}</div>
      ) : null}
    </div>
  );
}

export default memo(searchResultDropDown);
