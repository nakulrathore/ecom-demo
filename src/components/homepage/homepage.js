import React, { useEffect, useState } from "react";
import ProductCard from "../../components/product-card/product-card";

import { getProductByGroupsData } from "../../data/getData";
import "./homepage.scss";


function HomePage() {
  const [homePageData, setHomePageData] = useState({
    isLoading: true,
    data: [],
  });

  useEffect(() => {
    getProductByGroupsData(["iphone", "oneplus_phone"]).then((data) => {
      setHomePageData({
        isLoading: false,
        data,
      });
    });
  }, []);

  const renderProductGroup = (productGroup) => {
    const { productGroupDesc, products } = productGroup;

    return (
      <div className="product-group">
        <h2>{productGroupDesc}</h2>
        <section className="product-list">
          {products.map((product) => {
            const { title, cost, images } = product;
            const productImage = require(`../../assets/images/${images[0]}.jpg`)
              .default;

            return (
              <ProductCard
                title={title}
                cost={cost}
                productImage={productImage}
              />
            );
          })}
        </section>
      </div>
    );
  };

  const { isLoading } = homePageData;
  return (
    <section className="body-wrapper">
      {isLoading ? (
        <div class="loader"></div>
      ) : (
        homePageData.data.map((oneGroupData) => {
          return renderProductGroup(oneGroupData);
        })
      )}
    </section>
  );
}

export default HomePage;
