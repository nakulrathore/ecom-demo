import React from "react";
import "./product-card.scss";

function ProductCard({ title, productImage, cost }) {
  return (
    <div key={title} className="product">
      <img src={productImage} alt={title} />
      <h3>{title}</h3>
      <h4>₹{cost}</h4>
    </div>
  );
}

export default ProductCard;
