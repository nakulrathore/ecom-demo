import { Link } from "react-router-dom";
import Search from "../search/search";

import "./header.scss";

function Header() {
  return (
    <header className="header-wrapper">
      <Link to="/" className="logo">
        InstaShop
      </Link>
      <Search />
      <ul className="nav-wrapper">
        <li>
          <button className="contact">Contact Us</button>
        </li>
      </ul>
    </header>
  );
}

export default Header;
